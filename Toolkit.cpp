#include "Toolkit.h"


double Toolkit::Show_AO()
{
	std::cout<<V.rows()<<"   "<<N.rows()<<std::endl;


	double AO_I = 0.;
	for (int i = 0; i < AO.rows(); i++)
	{
		AO_I += AO(i)/AO.rows();
	}

	printf("AO number is : %.4f\n", AO_I);
	printf("Last AO number is : %.4f\n",AO(AO.rows()-1));
	return AO_I;
}

double Toolkit::AO_Integrate(std::vector<Point> list)
{
	if(list.size()==0) { return -1 ;}
	double result = 0.;
	for(int i = 0 ; i < list.size() ; i++)
	{
		Primitive_id p_id = tree.closest_point_and_primitive(list[i]).second;
		vertex_handle closest = p_id->halfedge()->vertex();
		std::map<vertex_handle,double>::iterator iter = AO_MAP.find(closest);
		if(iter!=AO_MAP.end())
		{
			result += (iter->second / list.size());
		}
	}
	return result;
}

Toolkit::Point Toolkit::refl(Point input,Plane pl)
{
	const double x = input.x();
	const double y = input.y();
	const double z = input.z();
	const double t = (pl.a()*x+pl.b()*y+pl.c()*z+pl.d())/(pl.a()*pl.a()+pl.b()*pl.b()+pl.c()*pl.c());
	return Point(x-2*pl.a()*t,y-2*pl.b()*t,z-2*pl.c()*t);
}

int Toolkit::Find_Reflect()
{
	typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron>         Facet_Primitive;
	typedef CGAL::AABB_traits<Kernel, Facet_Primitive>                  Facet_Traits;
	typedef CGAL::AABB_tree<Facet_Traits>                               Facet_tree;
	printf("\n");
	std::ifstream ref = std::ifstream("reflect.dat");
	Facet_tree *face_tree = new Facet_tree;
	Point center = Point(0,0,0);
	Polyhedron cpy;
	char* buf = new char[100];
	std::vector<Point> vt_p;
	// Calculate centroid vertex
	int nb_vertex = mesh.size_of_vertices();
	for(vertex_iterator it = mesh.vertices_begin(),end = mesh.vertices_end();it!=end;it++)
	{
		center = Point((*it).point().x()/nb_vertex +center.x(),(*it).point().y()/nb_vertex +center.y()
			,(*it).point().z()/nb_vertex +center.z());
		vt_p.push_back((*it).point());
	}
	double res[3] = {0.};
	double max_v = 9999;
	while(ref.getline(buf,100))
	{
		double x,y,z;
		sscanf(buf,"%lf%lf%lf",&x,&y,&z);
		printf("...");
		// Construct plane
		Plane pl = Plane(center,Vector(x,y,z));
		double min_v = -9999;
		cpy = mesh;
		for(vertex_iterator it = cpy.vertices_begin(),end = cpy.vertices_end();it!=end;it++)
		{
			(*it).point() = refl((*it).point(),pl);
		}
		face_tree->clear();
		face_tree->rebuild(faces(cpy).first, faces(cpy).second, cpy);
		face_tree->accelerate_distance_queries();
		for(std::vector<Point>::iterator it = vt_p.begin(),end = vt_p.end();it!=end;it++)
		{
			Point closest = face_tree->closest_point((*it));
			const double dis = CGAL::to_double(((*it).x()-closest.x())*((*it).x()-closest.x()) + 
				((*it).y()-closest.y())*((*it).y()-closest.y()) + 
				((*it).z()-closest.z())*((*it).z()-closest.z())) ;
			if(dis > min_v)
				min_v = dis;
		}
		if(min_v < max_v)
		{
			max_v = min_v;
			res[0] = x ;
			res[1] = y ;
			res[2] = z ;
		}
	}
	if(max_v > 2000) return 0;		// 2000为阈值，返回值为0表示没有找到对称平面
	Symm = Plane(center,Vector(res[0],res[1],res[2]));
	printf("\nSymmetric plane parameters :  %.2f  %.2f  %.2f \n",res[0],res[1],res[2]);
	printf("Symmetric value : %.4f\n",max_v);
	ref.close();
	return 1;
}

double Toolkit::Volume_OBB(Polyhedron & poly)
{
	std::vector<Point> points;
	points.reserve(poly.size_of_vertices());
	for(Polyhedron::Point_const_iterator it = poly.points_begin();
		it != poly.points_end();it ++)
	{
		points.push_back(*it);
	}
	Polyhedron CH3;
	CGAL::convex_hull_3(points.begin(),points.end(),CH3);

	printf("CH_3 size is %d\n",CH3.size_of_vertices());
	FT V_Min = 10000000.0;
	Polygon_2 convex_hull,min_rectangle;
	
	// Calculate OBB using Rotating Calipers
	for(facet_iterator it = CH3.facets_begin(),nd = CH3.facets_end();
		it != nd; it++)
	{
		Vector normal = CGAL::NULL_VECTOR;
		Point e = it->halfedge()->vertex()->point();
		HF_circulator he = it->facet_begin();
		HF_circulator end = he;
		CGAL_For_all(he,end)
		{
			const Point& prev = he->prev()->vertex()->point();
			const Point& curr = he->vertex()->point();
			const Point& next = he->next()->vertex()->point();
			const Vector n = CGAL::cross_product(next-curr,prev-curr);
			normal = normal + n;
		}
		normal = normal / std::sqrt(normal.squared_length());
		// 		std::cout << " Normal vector is "<<normal<<"  Length is "<<normal.squared_length()<<std::endl;
		// 		system("pause");
		std::vector<Point_2> points;
		Plane p = Plane(e,normal);
		Vector basis[2];
		// 根据平面求平面上的两个正交向量 basis[0] 和 basis[1]
		// base[2]  为平面的垂线
		const FT base_0 = p.base1().squared_length();
		const FT base_1 = p.base2().squared_length(); 
		const FT base_2 = p.orthogonal_vector().squared_length();
		// 基向量
		basis[0] = p.base1()/sqrt(base_0);
		basis[1] = p.base2()/sqrt(base_1);
		double Dis_M = 0.0 ;
		for(vertex_iterator vt = CH3.vertices_begin(),vd = CH3.vertices_end();
			vt != vd ; vt++)
		{
			const Vector terminal = Vector(vt->point().x()-e.x(),vt->point().y()-e.y(),vt->point().z()-e.z());
			Point_2 pt = Point_2(basis[0]*terminal,basis[1]*terminal);
			points.push_back(pt);
			double Dis = - normal*terminal;
			if(Dis < -0.1 ) 
			{
				Dis = 10000000.;			// Infinite
				//printf("过小\n");
			}
			if(Dis > Dis_M)
			{
				Dis_M = Dis;
			}
		}
		convex_hull.clear();
		CGAL::convex_hull_2(points.begin(), points.end(), std::back_inserter(convex_hull));
		min_rectangle.clear();
		CGAL::min_rectangle_2(convex_hull.vertices_begin(), convex_hull.vertices_end(), std::back_inserter(min_rectangle));
		double V =fabs(min_rectangle.area()) * Dis_M;
		if(V < V_Min)
		{
			V_Min = V;
		}
	}
	printf("%.4f\n",V_Min);
	return V_Min;
}

void Toolkit::mytest()
{
	std::vector<Point> test;
	test.push_back(Point(1,1,1));
	test.push_back(Point(2,2,2));
	printf("%.4f\n",AO_Integrate(test));
	return;
}

void Toolkit::Open_File(std::string filename)
{
	if(edgefile == NULL )
	{
		edgefile.open(filename,std::ios::out|std::ios::app);
	}
	else
	{
		edgefile.close();
		edgefile.open(filename,std::ios::out|std::ios::app);
	}
}

void Toolkit::Close_File()
{
	if(edgefile != NULL)
	{
		edgefile.close();
	}
}

void Toolkit::Clear_File(std::string filename)
{
	if(edgefile != NULL)
	{
		edgefile.close();
		edgefile.open(filename);
		edgefile.close();
		Open_File(filename);
	}
	else
	{
		edgefile.open(filename);
		edgefile.close();
		Open_File(filename);
	}
}

void Toolkit::Dimension(Point s,Point t,Plane pl,Point pop)
{
	Vector basis[2];
	const FT base_0 = pl.base1().squared_length();
	const FT base_1 = pl.base2().squared_length(); 
	basis[0] = pl.base1()/sqrt(base_0);
	basis[1] = pl.base2()/sqrt(base_1);
	char buf[1000];
	const Vector ter1 = Vector(s.x()-pop.x(),s.y()-pop.y(),s.z()-pop.z());
	const Vector ter2 = Vector(t.x()-pop.x(),t.y()-pop.y(),t.z()-pop.z());
	sprintf(buf,"%.4f %.4f %.4f %.4f\n",ter1*basis[0],ter1*basis[1],ter2*basis[0],ter2*basis[1]);
	edgefile<<buf;
}

void Toolkit::Dimension(Point_2 s,Point_2 t)
{
	char buf[1000];
	sprintf(buf,"%.4f %.4f %.4f %.4f\n",s.x(),s.y(),t.x(),t.y());
	edgefile<<buf;
}
void Toolkit::Init_Num(int n)
{
	char buf[100];
	sprintf(buf,"%d\n",n);
	edgefile<<buf;
}

template <class Facet, class Kernel>
typename Kernel::Vector_3 compute_facet_normal(const Facet& f)
{
	typedef typename Kernel::Point_3 Point;
	typedef typename Kernel::Vector_3 Vector;
	typedef typename Facet::Halfedge_around_facet_const_circulator HF_circulator;
	Vector normal = CGAL::NULL_VECTOR;
	HF_circulator he = f.facet_begin();
	HF_circulator end = he;
	CGAL_For_all(he,end)
	{
		const Point& prev = he->prev()->vertex()->point();
		const Point& curr = he->vertex()->point();
		const Point& next = he->next()->vertex()->point();
		Vector n = CGAL::cross_product(next-curr,prev-curr);
		normal = normal + (n / std::sqrt(n*n));
	}
	return normal / std::sqrt(normal * normal);
}

void Toolkit::Search_S(Plane pl)
{
	if(S.size()) { S.clear(); }
	Vector normal;
	const Vector normal_pl = pl.orthogonal_vector()/ std::sqrt(pl.orthogonal_vector().squared_length());
	for(vertex_iterator it = mesh.vertices_begin(); it!=mesh.vertices_end();
		it ++ )
	{
		// Compute normal of vertex
		normal = CGAL::NULL_VECTOR;
		HV_circulator he = it->vertex_begin();
		HV_circulator end = he;
		CGAL_For_all(he,end)
		{
			if(!he->is_border())
			{
				Vector n = compute_facet_normal<Facet,Kernel>(*he->facet());
				normal = normal + (n / std::sqrt(n*n));
			}
		}
		normal = normal / std::sqrt(normal * normal);
		
		if(fabs(normal*normal_pl) < 0.95)
		{
			S.push_back(it->point());
		}
	}
}

#define Tollerance 0.05
bool Toolkit::Search_SFra(Plane pl)
{
	const Vector normal_pl = pl.orthogonal_vector()/ std::sqrt(pl.orthogonal_vector().squared_length());
	for(std::vector<Point>::iterator it = S.begin();it!=S.end();it++)
	{
		const Vector origin = Vector(it->x()-pl.point().x(),it->y()-pl.point().y(),it->z()-pl.point().z());
		if(origin * normal_pl < Tollerance)
		{
			return true;
		}
	}
	return false;
}

bool Toolkit::Initial_Printer(Polyhedron& poly)
{
	typedef Polyhedron::Facet_iterator kl;
	// Calculate AABB Box

	// Modify vertex coordinates

	// Repair 
	return false;
}

void Toolkit::Load_Edgefile(std::string filename)
{
	cdt.clear();		// Rebuild constrained triangulation object
	std::ifstream ifs(filename);
	bool first=true;
	int n;
	ifs >> n;

	K::Point_2 p,q, qold;

	CDT::Vertex_handle vp, vq, vqold;
	while(ifs >> p) 
	{
		ifs >> q;
		if(p == q)
		{
			continue;
		}
		if((!first) && (p == qold))
		{
			vp = vqold;
		} 
		else 
		{
			vp = cdt.insert(p);
		}
		vq = cdt.insert(q, vp->face());
		if(vp != vq) 
		{
			cdt.insert_constraint(vp,vq);
		}
		qold = q;
		vqold = vq;
		first = false;
	}
	initializeID(cdt);
	discoverComponents(cdt);
}

void Toolkit::discoverComponents(const CDT & ct)
{
	if (ct.dimension()!=2) return;
	int index = 0;
	std::list<CDT::Edge> border;
	discoverComponent(ct, ct.infinite_face(), index++, border);
	while(! border.empty())
	{
		CDT::Edge e = border.front();
		border.pop_front();
		Face_handle n = e.first->neighbor(e.second);
		if(n->counter() == -1)
		{
			discoverComponent(ct, n, e.first->counter()+1, border);
		}
	}
}

void Toolkit::discoverComponent(const CDT & ct, Face_handle start,int index, std::list<CDT::Edge>& border )

{
	if(start->counter() != -1)
	{
		return;
	}
	std::list<Face_handle> queue;
	queue.push_back(start);

	while(! queue.empty())
	{
		Face_handle fh = queue.front();
		queue.pop_front();
		if(fh->counter() == -1)
		{
			fh->counter() = index;
			fh->set_in_domain(index%2 == 1);
			for(int i = 0; i < 3; i++)
			{
				CDT::Edge e(fh,i);
				Face_handle n = fh->neighbor(i);
				if(n->counter() == -1)
				{
					if(ct.is_constrained(e))
					{
						border.push_back(e);
					} 
					else 
					{
						queue.push_back(n);
					}
				}
			}
		}
	}
}

void Toolkit::initializeID(const CDT& ct)
{
	for(All_faces_iterator it = ct.all_faces_begin();
		it != ct.all_faces_end(); 
		it++)
	{
		it->set_counter(-1);
	}
}