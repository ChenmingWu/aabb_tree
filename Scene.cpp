#include "Scene.h"
#include <iostream>

#include <QtCore/qglobal.h>
#include <QString>
#include <QTextStream>
#include <QFileInfo>
#include <QInputDialog>
/*#include "Fillholes.h"*/
#include "Refiner.h"
#include "render_edges.h"
#include "axisb.h"
#include <CGAL/Timer.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Subdivision_method_3.h>
#include <CGAL/polyhedron_cut_plane_3.h>
#include <time.h>
#include <fstream>
// constants
const int slow_distance_grid_size = 100;
const int fast_distance_grid_size = 20;

Scene::Scene()
  : m_frame (new ManipulatedFrame())
  , m_view_plane(false)
  , m_grid_size(slow_distance_grid_size)
  , m_cut_plane(NONE)
{
    m_pPolyhedron = NULL;
	neg_polyhedron = new Polyhedron;
	pos_polyhedron = new Polyhedron;
	tree = new BSP;

    // view options
    m_view_points = true;
    m_view_segments = true;
    m_view_polyhedron = true;

    // distance function
    m_red_ramp.build_red();
    m_blue_ramp.build_blue();
    m_max_distance_function = (FT)0.0;
}

Scene::~Scene()
{
	delete tree;
	delete neg_polyhedron;
	delete pos_polyhedron;
    delete m_pPolyhedron;
    delete m_frame;
	delete tkt;
}

int Scene::open(QString filename)
{
    QTextStream cerr(stderr);
    cerr << QString("Opening file \"%1\"\n").arg(filename);
    QApplication::setOverrideCursor(QCursor(::Qt::WaitCursor));

    QFileInfo fileinfo(filename);
    std::ifstream in(filename.toUtf8());
	tkt = new Toolkit(filename.toStdString());
    if(!in || !fileinfo.isFile() || ! fileinfo.isReadable())
    {
        std::cerr << "unable to open file" << std::endl;
        QApplication::restoreOverrideCursor();
        return -1;
    }

    if(m_pPolyhedron != NULL)
        delete m_pPolyhedron;

    // allocate new polyhedron
    m_pPolyhedron = new Polyhedron;
    in >> *m_pPolyhedron;
    if(!in)
    {
        std::cerr << "invalid OFF file" << std::endl;
        QApplication::restoreOverrideCursor();

        delete m_pPolyhedron;
        m_pPolyhedron = NULL;

        return -1;
    }
    
    // clear tree
    clear_internal_data();

    QApplication::restoreOverrideCursor();
	in.close();
    return 0;
}

void Scene::update_bbox()
{
    std::cout << "Compute bbox...";
    m_bbox = Bbox();

    if(m_pPolyhedron == NULL)
    {
        std::cout << "failed (no polyhedron)." << std::endl;
        return;
    }

    if(m_pPolyhedron->empty())
    {
        std::cout << "failed (empty polyhedron)." << std::endl;
        return;
    }

    Polyhedron::Point_iterator it = m_pPolyhedron->points_begin();
    m_bbox = (*it).bbox();
    for(; it != m_pPolyhedron->points_end();it++)
        m_bbox = m_bbox + (*it).bbox();
    std::cout << "done (" << m_pPolyhedron->size_of_facets()
        << " facets)" << std::endl;
	double in_v =(m_bbox.xmax()-m_bbox.xmin())*(m_bbox.ymax()-m_bbox.ymin())*(m_bbox.zmax()-m_bbox.zmin());

}

void Scene::draw()
{
    if(m_view_plane)
        ::glEnable(GL_DEPTH_TEST);
    else
        ::glDisable(GL_DEPTH_TEST);
  
    if(m_view_polyhedron)
        draw_polyhedron();

    if(m_view_points)
        draw_points();

    if(m_view_segments)
        draw_segments();

    if (m_view_plane)
    {
        switch( m_cut_plane )
        {
          case UNSIGNED_EDGES:
          case UNSIGNED_FACETS:
              draw_distance_function(m_thermal_ramp, m_thermal_ramp);
              break;
          case SIGNED_FACETS:
              draw_distance_function(m_red_ramp, m_blue_ramp);
              break;
          case CUT_SEGMENTS:
              draw_cut_segment_plane();
              break;
          case NONE: // do nothing
              break;
        }
    }
}

void Scene::draw_polyhedron()
{
    // draw black edges
    if(m_pPolyhedron != NULL)
    {
        ::glEnable(GL_LIGHTING);
        ::glColor3ub(255,0,255);
        ::glLineWidth(1.2f);
        gl_render_edges(*m_pPolyhedron);
    }
}

void Scene::draw_segments()
{
    if(m_segments.size() != 0)
    {
        ::glDisable(GL_LIGHTING);
        ::glColor3ub(0,100,0);
        ::glLineWidth(2.0f);
        ::glBegin(GL_LINES);
        std::list<Segment>::iterator it;
        for(it = m_segments.begin(); it != m_segments.end(); it++)
        {
            const Segment& s = *it;
            const Point& p = s.source();
            const Point& q = s.target();
            ::glVertex3d(p.x(),p.y(),p.z());
            ::glVertex3d(q.x(),q.y(),q.z());
        }
        ::glEnd();
    }
}

void Scene::draw_points()
{
    // draw red points
    if(m_points.size() != 0)
    {
        ::glDisable(GL_LIGHTING);
        ::glColor3ub(180,0,0);
        ::glPointSize(2.0f);
        ::glBegin(GL_POINTS);
        std::list<Point>::iterator it;
        for(it = m_points.begin(); it != m_points.end(); it++)
        {
            const Point& p = *it;
            ::glVertex3d(p.x(),p.y(),p.z());
        }
        ::glEnd();
    }
}

void Scene::draw_distance_function(const Color_ramp& ramp_pos,
                                   const Color_ramp& ramp_neg) const
{
    ::glDisable(GL_LIGHTING);
    if ( m_fast_distance ) { ::glShadeModel(GL_FLAT); }
    else { ::glShadeModel(GL_SMOOTH); }
    
    ::glBegin(GL_QUADS);
    int i,j;
    const int nb_quads = m_grid_size-1;
    for(i=0;i<nb_quads;i++)
    {
        for(j=0;j<nb_quads;j++)
        {
            const Point_distance& pd00 = m_distance_function[i][j];
            const Point_distance& pd01 = m_distance_function[i][j+1];
            const Point_distance& pd11 = m_distance_function[i+1][j+1];
            const Point_distance& pd10 = m_distance_function[i+1][j];
            const Point& p00 = pd00.first;
            const Point& p01 = pd01.first;
            const Point& p11 = pd11.first;
            const Point& p10 = pd10.first;
            const FT& d00 = pd00.second;
            const FT& d01 = pd01.second;
            const FT& d11 = pd11.second;
            const FT& d10 = pd10.second;
            
            // determines grey level
            unsigned int i00 = 255-(unsigned)(255.0 * (double)std::fabs(d00) / m_max_distance_function);
            unsigned int i01 = 255-(unsigned)(255.0 * (double)std::fabs(d01) / m_max_distance_function);
            unsigned int i11 = 255-(unsigned)(255.0 * (double)std::fabs(d11) / m_max_distance_function);
            unsigned int i10 = 255-(unsigned)(255.0 * (double)std::fabs(d10) / m_max_distance_function);
            
            // assembles one quad
            if(d00 > 0.0)
                ::glColor3ub(ramp_pos.r(i00),ramp_pos.g(i00),ramp_pos.b(i00));
            else
                ::glColor3ub(ramp_neg.r(i00),ramp_neg.g(i00),ramp_neg.b(i00));
            ::glVertex3d(p00.x(),p00.y(),p00.z());
            
            if(d01 > 0.0)
                ::glColor3ub(ramp_pos.r(i01),ramp_pos.g(i01),ramp_pos.b(i01));
            else
                ::glColor3ub(ramp_neg.r(i01),ramp_neg.g(i01),ramp_neg.b(i01));
            ::glVertex3d(p01.x(),p01.y(),p01.z());
            
            if(d11 > 0)
                ::glColor3ub(ramp_pos.r(i11),ramp_pos.g(i11),ramp_pos.b(i11));
            else
                ::glColor3ub(ramp_neg.r(i11),ramp_neg.g(i11),ramp_neg.b(i11));
            ::glVertex3d(p11.x(),p11.y(),p11.z());
            
            if(d10 > 0)
                ::glColor3ub(ramp_pos.r(i10),ramp_pos.g(i10),ramp_pos.b(i10));
            else
                ::glColor3ub(ramp_neg.r(i10),ramp_neg.g(i10),ramp_neg.b(i10));
            ::glVertex3d(p10.x(),p10.y(),p10.z());
        }
    }
    ::glEnd();
}

void Scene::draw_cut_segment_plane() const
{
    float diag = .6f * float(bbox_diag());
	
    ::glDisable(GL_LIGHTING);
    ::glLineWidth(1.0f);
    ::glColor3f(.6f, .6f, .6f);

    // draw grid
    ::glPushMatrix();
    ::glMultMatrixd(m_frame->matrix());
    //QGLViewer::drawGrid(diag);
    ::glPopMatrix();
	
    // draw cut segments
    ::glLineWidth(2.0f);
    ::glColor3f(1.f, 0.f, 0.f);
	
    ::glBegin(GL_LINES);
    for ( std::vector<Segment>::const_iterator it = m_cut_segments.begin(), 
          end = m_cut_segments.end() ; it != end ; ++it )
    {
        const Point& a = it->source();
        const Point& b = it->target();
      
        ::glVertex3d(a.x(), a.y(), a.z());
        ::glVertex3d(b.x(), b.y(), b.z());
    }
    ::glEnd();
	/*
	
	std::vector<Segment>::const_iterator it = m_cut_segments.begin(),end = m_cut_segments.end() - 3;
	do{
	::glBegin(GL_QUADS);
		const Point& a = it->source();
		const Point& b = it->target();
		it++;const Point& c = it->target();
		it++;const Point& d = it->target();
        ::glVertex3d(a.x(), a.y(), a.z());
        ::glVertex3d(b.x(), b.y(), b.z());
        ::glVertex3d(c.x(), c.y(), c.z());
        ::glVertex3d(d.x(), d.y(), d.z());
	::glEnd();
	++it;
	}
	while(it<=end);
    	
    //draw neighbour segments high-light
    ::glLineWidth(2.0f);
    ::glColor3f(0.f, 1.f, 1.f);
    ::glBegin(GL_LINES);
	
	typedef Polyhedron::Facet_const_iterator                          Facet_iterator;
	for ( Facet_iterator it = m_facet_handles.begin() )
    {
        const Point& a = it->halfedge()->vertex()->point();
        const Point& b = it->halfedge()->next()->vertex()->point();
      	const Point& c = it->halfedge()->next()->next()->vertex()->point();
        ::glVertex3d(a.x(), a.y(), a.z());
        ::glVertex3d(b.x(), b.y(), b.z());
        ::glVertex3d(c.x(), c.y(), c.z());
    }
    ::glEnd();
	*/
  /*
    // fill grid with transparent blue
    ::glPushMatrix();
    ::glMultMatrixd(m_frame->matrix());
    ::glColor4f(.6f, .85f, 1.f, .65f);

    ::glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); 
    ::glEnable(GL_BLEND);
    ::glBegin(GL_QUADS);
    ::glVertex3d(-diag, -diag, 0.);
    ::glVertex3d(-diag,  diag, 0.);
    ::glVertex3d( diag,  diag, 0.);
    ::glVertex3d( diag, -diag, 0.);
    ::glEnd();
    ::glDisable(GL_BLEND);
  
    ::glPopMatrix();
    */
}

FT Scene::random_in(const double a,
                    const double b)
{
    double r = rand() / (double)RAND_MAX;
    return (FT)(a + (b - a) * r);
}

Point Scene::random_point(const CGAL::Bbox_3& bbox)
{
    FT x = random_in(bbox.xmin(),bbox.xmax());
    FT y = random_in(bbox.ymin(),bbox.ymax());
    FT z = random_in(bbox.zmin(),bbox.zmax());
    return Point(x,y,z);
}

Vector Scene::random_vector()
{
    FT x = random_in(0.0,1.0);
    FT y = random_in(0.0,1.0);
    FT z = random_in(0.0,1.0);
    return Vector(x,y,z);
}

Ray Scene::random_ray(const CGAL::Bbox_3& bbox)
{
    Point p = random_point(bbox);
    Point q = random_point(bbox);
    return Ray(p,q);
}

Segment Scene::random_segment(const CGAL::Bbox_3& bbox)
{
    Point p = random_point(bbox);
    Point q = random_point(bbox);
    return Segment(p,q);
}

Line Scene::random_line(const CGAL::Bbox_3& bbox)
{
    Point p = random_point(bbox);
    Point q = random_point(bbox);
    return Line(p,q);
}

Plane Scene::random_plane(const CGAL::Bbox_3& bbox)
{
    Point p = random_point(bbox);
    Vector vec = random_vector();
    return Plane(p,vec);
}

Plane Scene::frame_plane() const
{
    const qglviewer::Vec& pos = m_frame->position();
    const qglviewer::Vec& n = m_frame->inverseTransformOf(qglviewer::Vec(0.f, 0.f, 1.f));
		Plane pl = Plane( Point( 0.5, 0.0, 0.0),
                      Point( 0.0, 0.0, 1.5),
                      Point( 0.0, 0.5, 0.0));
    //return Plane(n[0], n[1],  n[2], - n * pos);
    return pl;
}

Aff_transformation Scene::frame_transformation() const
{
    const ::GLdouble* m = m_frame->matrix();
  
    // OpenGL matrices are row-major matrices
    return Aff_transformation (m[0], m[4], m[8], m[12],
                               m[1], m[5], m[9], m[13],
                               m[2], m[6], m[10], m[14]);
}

FT Scene::bbox_diag() const
{
  double dx = m_bbox.xmax()-m_bbox.xmin();
  double dy = m_bbox.ymax()-m_bbox.ymin();
  double dz = m_bbox.zmax()-m_bbox.zmin();
  
  return FT(std::sqrt(dx*dx + dy*dy + dz*dz));
}

void Scene::build_facet_tree()
{
    if ( NULL == m_pPolyhedron )
    {
        std::cerr << "Build facet tree failed: load polyhedron first." << std::endl;
        return;
    }

	if ( !m_facet_tree.empty() ) { m_facet_tree.clear();}
  
    // build tree
    CGAL::Timer timer;
    timer.start();
    //std::cout << "Construct Facet AABB tree...";
    m_facet_tree.rebuild(faces(*m_pPolyhedron).first, faces(*m_pPolyhedron).second,*m_pPolyhedron);
    m_facet_tree.accelerate_distance_queries();
    std::cout << "done (" << timer.time() << " s)" << std::endl;
}

void Scene::build_edge_tree()
{
    if ( NULL == m_pPolyhedron )
    {
        std::cerr << "Build edge tree failed: load polyhedron first." << std::endl;
        return;
    }
  
	if ( !m_edge_tree.empty() ) { m_edge_tree.clear(); }
    
    // build tree
    CGAL::Timer timer;
    timer.start();
    //std::cout << "Construct Edge AABB tree...";
    m_edge_tree.rebuild(edges(*m_pPolyhedron).first,edges(*m_pPolyhedron).second,*m_pPolyhedron);
    m_edge_tree.accelerate_distance_queries();
    std::cout << "done (" << timer.time() << " s)" << std::endl;
}

void Scene::clear_internal_data()
{
    m_facet_tree.clear();
    m_edge_tree.clear();

    clear_points();
    clear_segments();
    clear_cutting_plane();
}

void Scene::clear_cutting_plane()
{
    m_cut_segments.clear();
    m_cut_plane = NONE;
  
    deactivate_cutting_plane();
}

void Scene::update_grid_size()
{
    m_grid_size = m_fast_distance ? fast_distance_grid_size
                                  : slow_distance_grid_size;
}

void Scene::generate_points_in(const unsigned int nb_points,
                               const double min,
                               const double max)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }

    typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
    typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
    typedef CGAL::AABB_tree<Traits> Tree;

    std::cout << "Construct AABB tree...";
    Tree tree(faces(*m_pPolyhedron).first, faces(*m_pPolyhedron).second, *m_pPolyhedron);
    std::cout << "done." << std::endl;

    CGAL::Timer timer;
    timer.start();
    std::cout << "Generate " << nb_points << " points in interval ["
        << min << ";" << max << "]";

    unsigned int nb_trials = 0;
    Vector vec = random_vector();
    while(m_points.size() < nb_points)
    {
        Point p = random_point(tree.bbox());

        // measure distance
        FT signed_distance = std::sqrt(tree.squared_distance(p));

        // measure sign
        Ray ray(p,vec);
        int nb_intersections = (int)tree.number_of_intersected_primitives(ray);
        if(nb_intersections % 2 != 0)
            signed_distance *= -1.0;

        if(signed_distance >= min &&
            signed_distance <= max)
        {
            m_points.push_back(p);
            if(m_points.size()%(nb_points/10) == 0)
                std::cout << "."; // ASCII progress bar
        }
        nb_trials++;
    }
    double speed = (double)nb_trials / timer.time();
    std::cout << "done (" << nb_trials << " trials, "
        << timer.time() << " s, "
        << speed << " queries/s)" << std::endl;
}


void Scene::generate_inside_points(const unsigned int nb_points)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }

    typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
    typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
    typedef CGAL::AABB_tree<Traits> Tree;

    std::cout << "Construct AABB tree...";
    Tree tree(faces(*m_pPolyhedron).first, faces(*m_pPolyhedron).second,*m_pPolyhedron);
    std::cout << "done." << std::endl;

    CGAL::Timer timer;
    timer.start();
    std::cout << "Generate " << nb_points << " inside points";

    unsigned int nb_trials = 0;
    Vector vec = random_vector();
    while(m_points.size() < nb_points)
    {
        Point p = random_point(tree.bbox());
        Ray ray(p,vec);
        int nb_intersections = (int)tree.number_of_intersected_primitives(ray);
        if(nb_intersections % 2 != 0)
        {
            m_points.push_back(p);
            if(m_points.size()%(nb_points/10) == 0)
                std::cout << "."; // ASCII progress bar
        }
        nb_trials++;
    }
    double speed = (double)nb_trials / timer.time();
    std::cout << "done (" << nb_trials << " trials, "
        << timer.time() << " s, "
        << speed << " queries/s)" << std::endl;
}

void Scene::generate_boundary_segments(const unsigned int nb_slices)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }

    typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
    typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
    typedef CGAL::AABB_tree<Traits> Tree;
    typedef Tree::Object_and_primitive_id Object_and_primitive_id;

    std::cout << "Construct AABB tree...";
    Tree tree(faces(*m_pPolyhedron).first,faces(*m_pPolyhedron).second,*m_pPolyhedron);
    std::cout << "done." << std::endl;

    CGAL::Timer timer;
    timer.start();
    std::cout << "Generate boundary segments from " << nb_slices << " slices: ";

    Vector normal((FT)0.0,(FT)0.0,(FT)1.0);
    unsigned int i;

    const double dz = m_bbox.zmax() - m_bbox.zmin();
    for(i=0;i<nb_slices;i++)
    {
        FT z = m_bbox.zmin() + (FT)i / (FT)nb_slices * dz;
        Point p((FT)0.0, (FT)0.0, z);
        Plane plane(p,normal);

        std::list<Object_and_primitive_id> intersections;
        tree.all_intersections(plane,std::back_inserter(intersections));

        std::list<Object_and_primitive_id>::iterator it;
        for(it = intersections.begin();
            it != intersections.end();
            it++)
        {
            Object_and_primitive_id op = *it;
            CGAL::Object object = op.first;
            Segment segment;
            if(CGAL::assign(segment,object))
                m_segments.push_back(segment);
        }
    }
    std::cout << m_segments.size() << " segments, " << timer.time() << " s." << std::endl;
}

void Scene::generate_boundary_points(const unsigned int nb_points)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }

    typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
    typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
    typedef CGAL::AABB_tree<Traits> Tree;
    typedef Tree::Object_and_primitive_id Object_and_primitive_id;

    std::cout << "Construct AABB tree...";
    Tree tree(faces(*m_pPolyhedron).first, faces(*m_pPolyhedron).second,*m_pPolyhedron);
    std::cout << "done." << std::endl;

    CGAL::Timer timer;
    timer.start();
    std::cout << "Generate boundary points: ";

    unsigned int nb = 0;
    unsigned int nb_lines = 0;
    while(nb < nb_points)
    {
        Line line = random_line(tree.bbox());

        std::list<Object_and_primitive_id> intersections;
        tree.all_intersections(line,std::back_inserter(intersections));
        nb_lines++;

        std::list<Object_and_primitive_id>::iterator it;
        for(it = intersections.begin();
            it != intersections.end();
            it++)
        {
            Object_and_primitive_id op = *it;
            CGAL::Object object = op.first;
            Point point;
            if(CGAL::assign(point,object))
            {
                m_points.push_back(point);
                nb++;
            }
        }
    }
    std::cout << nb_lines << " line queries, " << timer.time() << " s." << std::endl;
}

void Scene::generate_edge_points(const unsigned int nb_points)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }

    typedef CGAL::AABB_halfedge_graph_segment_primitive<Polyhedron> Primitive;
    typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
    typedef CGAL::AABB_tree<Traits> Tree;
    typedef Tree::Object_and_primitive_id Object_and_primitive_id;

    std::cout << "Construct AABB tree...";
    Tree tree( CGAL::edges(*m_pPolyhedron).first,
               CGAL::edges(*m_pPolyhedron).second,
               *m_pPolyhedron);
    std::cout << "done." << std::endl;

    CGAL::Timer timer;
    timer.start();
    std::cout << "Generate edge points: ";

    unsigned int nb = 0;
    unsigned int nb_planes = 0;
    while(nb < nb_points)
    {
        Plane plane = random_plane(tree.bbox());

        std::list<Object_and_primitive_id> intersections;
        tree.all_intersections(plane,std::back_inserter(intersections));
        nb_planes++;

        std::list<Object_and_primitive_id>::iterator it;
        for(it = intersections.begin();
            it != intersections.end();
            it++)
        {
            Object_and_primitive_id op = *it;
            CGAL::Object object = op.first;
            Point point;
            if(CGAL::assign(point,object))
            {
                m_points.push_back(point);
                nb++;
            }
        }
    }
    std::cout << nb_planes << " plane queries, " << timer.time() << " s." << std::endl;
}

template <typename Tree>
void Scene::compute_distance_function(const Tree& tree)
{
    // Get transformation
    Aff_transformation t = frame_transformation();
    
    m_max_distance_function = FT(0);
    FT diag = bbox_diag();
    
    const FT dx = diag;
    const FT dy = diag;
    const FT z (0);
    
    for(int i=0 ; i<m_grid_size ; ++i)
    {
        FT x = -diag/FT(2) + FT(i)/FT(m_grid_size) * dx;
        
        for(int j=0 ; j<m_grid_size ; ++j)
        {
            FT y = -diag/FT(2) + FT(j)/FT(m_grid_size) * dy;
            
            Point query = t( Point(x,y,z) );
            FT dist = CGAL::sqrt( tree.squared_distance(query) );
            
            m_distance_function[i][j] = Point_distance(query,dist);
            m_max_distance_function = (std::max)(dist, m_max_distance_function);
        }
    }
}

template <typename Tree>
void Scene::sign_distance_function(const Tree& tree)
{
    typedef typename Tree::size_type size_type;
    Vector random_vec = random_vector();
    
    for(int i=0 ; i<m_grid_size ; ++i)
    {
        for(int j=0 ; j<m_grid_size ; ++j)
        {
            const Point& p = m_distance_function[i][j].first;
            const FT unsigned_distance = m_distance_function[i][j].second;
            
            // get sign through ray casting (random vector)
            Ray ray(p, random_vec);
            size_type nbi = tree.number_of_intersected_primitives(ray);
            
            FT sign ( (nbi&1) == 0 ? 1 : -1);
            m_distance_function[i][j].second = sign * unsigned_distance;
        }
    }
}
 
void Scene::unsigned_distance_function()
{
    // Build tree (if build fail, exit)
    build_facet_tree();
    if ( m_facet_tree.empty() ) { return; }
  
    compute_distance_function(m_facet_tree);
    
    m_cut_plane = UNSIGNED_FACETS;
}

void Scene::unsigned_distance_function_to_edges()
{
    // Build tree (if build fail, exit)
    build_edge_tree();
    if ( m_edge_tree.empty() ) { return; }
    
    compute_distance_function(m_edge_tree);
    
    m_cut_plane = UNSIGNED_EDGES;
}

void Scene::signed_distance_function()
{
    // Build tree (if build fail, exit)
    build_facet_tree();
    if ( m_facet_tree.empty() ) { return; }
    
    compute_distance_function(m_facet_tree);
    sign_distance_function(m_facet_tree);

    m_cut_plane = SIGNED_FACETS;
}

bool negative(Plane p, Point a)
{
	if(p.a()*a.x() + p.b()*a.y() + p.c()*a.z() + p.d() > 1e-8)
		return true;
	else
		return false;
}

bool positive(Plane p, Point a)
{
	if(p.a()*a.x() + p.b()*a.y() + p.c()*a.z() + p.d() < - 1e-8)
		return true;
	else
		return false;
}

bool similarity(Point a,Point b)
{
	if( (a.x()-b.x())*(a.x()-b.x()) + (a.y()-b.y())*(a.y()-b.y()) + (a.z()-b.z())*(a.z()-b.z())
		<  1e-12)
	{
		return true;
	}
	else
		return false;
}

bool my_parallel(Point a,Point b,Point c)
{
	#define PRECISION 1e-10
	Point r1 = Point(10*(a.x() - b.x()),10*(a.y() - b.y()),10*(a.z() - b.z()));
	Point r2 = Point(10*(c.x() - b.x()),10*(c.y() - b.y()),10*(c.z() - b.z()));
	double res = fabs(r1.y()*r2.z()-r1.z()*r2.y())+fabs(r1.z()*r2.x()-r1.x()*r2.z())+fabs(r1.x()*r2.y()-r1.y()*r2.x());
	//printf("面积为 : %lf",res);
	if(res < PRECISION)
		return true;
	else
		return false;
}

void Scene::load_planes()
{
	if(candidate_planes.size()) { candidate_planes.clear(); }
	char buf[300];
	printf("Loading candidate planes ...");
	std::ifstream lplane= std::ifstream("reflect.dat");
	while(lplane.getline(buf,100))
	{
		printf(".");
		double x,y,z;
		sscanf(buf,"%lf%lf%lf",&x,&y,&z);
		candidate_planes.push_back(Plane(x,y,z,0));
	}
	printf("\nLoaded %d planes.",candidate_planes.size());
// 	for(int i = 1 ; i < 4 ; i ++)
// 	{
// 		for(int j = 1 ; j <4 ; j++)
// 		{
// 			for(int l = 1 ; l < 4 ; l ++)
// 			{
// 				candidate_planes.push_back(Plane(i/3.0,j/3.0,l/3.0,0));
// 			}
// 		}
// 	}
// 	std::cerr<<" Load " << candidate_planes.size() <<"candidate planes !"<<std::endl;
}

void Scene::cut_segment_plane()
{
	printf("You pressed cut model button , searching for symmetric plane ....");
	if(tkt->Find_Reflect())
	{
		printf("Symmetric plane existed.\n");
		//tkt->Symm;
	}
	else
	{
		printf("No symmetric plane for this model.\n");
	}
	load_planes();

	// Initialization completed
	BSP *node = new BSP;
	node->poly = *m_pPolyhedron;
	node->OBB = tkt->Volume_OBB(node->poly);
	node->score = 0.;
	node->V = 0.0;
	BSP_tree.insert(*node);
	printf("Input model's OBB volume is : %.4f\n",node->OBB);
	printf("Please import printer's volume :");
	scanf("%lf",&PRINTER_V);
	printf("OK...  Load cut planes...");
	// Put init_polyhedron into BSP Tree's node
	GLOBAL_F = false;
	delete node;
	int debug = 1 ;
	std::set<BSP> temp;
// 	double score = cut_with_plane(*node,Plane( Point( 0.5, 0.0, 0.0),Point( 0.0, 0.0, 0.5),Point( 0.0, 0.5, 0.0)),temp);
// 	btree.erase(*node);
// 	std::cerr<< "btree has " << btree.size() << " leafs" <<std::endl;
// 	std::cerr<< "Cut_result has " << result.size() << " roots" <<std::endl;
	//Judge whether all parts are smaller than printer's volume
	std::cerr<<BSP_tree.begin()->OBB<<std::endl;
	printf("Press ENTER to start...\n");
	system("pause");
	while(BSP_tree.begin()->OBB > PRINTER_V)
	{
		std::cerr<<"Cutting ... "<<std::endl;
		std::vector<Plane>::iterator selected;
		double MAXSCORE = 100000.0;		// Initialize 'MAXSCORE' as INFINITE value 
		double score = 0.;
		std::set<BSP> temp;
		for(std::vector<Plane>::iterator i = candidate_planes.begin(),
			id = candidate_planes.end();i!=id;i++)	//Use all plane to cut
		{
			if(!temp.empty())	{ temp.clear(); }
			for(std::set<BSP>::iterator it = BSP_tree.begin(), end = BSP_tree.end(); it!=end;
				it ++)
			{
				srand(int(time(0)));
				score = rand()%1000;
				if(cut_with_plane(*it,*i,temp) < -8000)
					break;
				if(score < MAXSCORE)
				{
					selected = i;			// After iterators , reserve the best plane
					MAXSCORE = score;		// Replace MAX Score
				}
			}
		}
		if(!temp.empty())	{ temp.clear(); }
		//Open flag 
		GLOBAL_F = true;
		for(std::set<BSP>::iterator it = BSP_tree.begin(), end = BSP_tree.end(); it!=end;
			it ++)
		{
			std::cerr<<"Final plane is " <<*selected<<std::endl;
			cut_with_plane(*it,*selected,temp);
		}
		BSP_tree.clear();
		BSP_tree = temp ;
		//Close flag
		GLOBAL_F = false;
		// Use selected plane to cut parts again
		std::cout<<"Score is " << MAXSCORE <<std::endl;
		// Push_back the resut and
		std::cout<<BSP_tree.size()<<std::endl;
		std::cout<<result.size()<<std::endl;
		candidate_planes.erase(selected);
	}
	//After BSP Search , export all mesh at the root directory
	time_t timer;
	tm* t_tm;
	time(&timer);
	t_tm = localtime(&timer);
 	char ROOT_DIC[30];
	const std::string off = ".off";
 	std::cerr<<"We reserved "<<result.size()<<" parts."<<std::endl;
	for(int x = 0 ; x < result.size() ; x ++)
	{
		sprintf(ROOT_DIC, "M%02dD%02d%02d%02d%02d-%d",t_tm->tm_mon+1, t_tm->tm_mday, t_tm->tm_hour, t_tm->tm_min, t_tm->tm_sec, x); 
 		std::ofstream out = std::ofstream(ROOT_DIC+off);
 		out << result[x].poly ;
 		out.close();
		std::cerr<<ROOT_DIC+off<<std::endl;
	}
	delete t_tm;
}

double Scene::cut_with_plane(BSP bsps , Plane plane , std::set<BSP>& btree)
{
	// Build tree (if build fail, exit)
	double cut_score = 0.0;
	std::cerr<<"Plane : " << plane<<std::endl;
	m_pPolyhedron->clear();
	*m_pPolyhedron = bsps.poly ;
	build_facet_tree();
	build_edge_tree();
	if ( m_facet_tree.empty() || m_edge_tree.empty() ) { system("pause"); }
	if ( myunion.size()) { myunion.clear(); }

	const Point pop = plane.point();	
	for(Polyhedron::Facet_iterator it=m_pPolyhedron->facets_begin(),end=m_pPolyhedron->facets_end();it!=end;it++)
	{
		if(!it->is_triangle())
		{
			std::cerr<<"Please make sure your input model is a tri-mesh model."<<std::endl;
			return -9000;
		}
	}
	// Compute intersections
	typedef std::vector<Facet_tree::Object_and_primitive_id> Intersections;
	typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
	typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
	typedef CGAL::AABB_tree<Traits> Tree;
	typedef Tree::Point_and_primitive_id Point_and_primitive_id;
	typedef Polyhedron::Halfedge_handle                          Halfedge_handle;
	Intersections intersections;
	Intersections connectors;
	if(!intersections.empty()) {intersections.clear(); }
	m_facet_tree.all_intersections(plane, std::back_inserter(intersections));
	// Fill data structure
	m_cut_segments.clear();
	//No intersection
	if(!intersections.size())
	{
		std::cerr<<"No intersection with plane "<<plane<<std::endl;
		return -9000;
	}
	// 2D -> 3D
	tkt->Clear_File("edge.edg");
	std::vector<Point> ao_points;
	if(ao_points.size()){ ao_points.clear(); }
	tkt->Init_Num(intersections.size());
	for ( Intersections::iterator it = intersections.begin(),
		end = intersections.end() ; it != end ; ++it )
	{
		const Segment* inter_seg = CGAL::object_cast<Segment>(&(it->first));
		if ( NULL != inter_seg )
		{			
			m_cut_segments.push_back(*inter_seg);
			Polyhedron::Face_handle f = it->second;
			MyUnion insert;
			inter_facet.insert(f);
			insert.facet = f;
			insert.source = inter_seg->source();
			insert.target = inter_seg->target();
			// 3D -> 2D Algorithm
			// Debug Mode
			//*****************************************************************************************************
			//std::cout<<"Source : "<<inter_seg->source()<<" Target : "<<inter_seg->target()<<std::endl;
			//*****************************************************************************************************
			tkt->Dimension(inter_seg->source(),inter_seg->target(),plane,pop);
			//tkt->Dimension(plane.to_2d(inter_seg->source()),plane.to_2d(inter_seg->target()));
			// AO Algorithm
			ao_points.push_back(inter_seg->source());
			ao_points.push_back(inter_seg->target());
			Halfedge_handle it = f->halfedge() ,end = f->halfedge();
			insert.handle = it;
			insert.flag = 0; //默认为0
			facet_set.empty();
			for(Polyhedron::Facet_iterator it=m_pPolyhedron->facets_begin(),end=m_pPolyhedron->facets_end();it!=end;it++)
			{
				facet_set.push_back(it);
			}
			do{
				if(similarity(inter_seg->source() , it->vertex()->point()))
				{
					insert.handle = it;
					if(insert.flag == 1 || insert.flag == 2)
					{
						insert.flag = 3;
						break;
					}	
					else
						insert.flag = 1;
				}
				if(similarity(inter_seg->target() , it->vertex()->point()))
				{
					insert.handle = it;
					if(insert.flag == 1 || insert.flag == 2)
					{
						insert.flag = 3; 
						break;
					}
					else
						insert.flag = 2;
				}
				it = it->next();
			}while( it != end);
			myunion.push_back(insert);
		}
	}
	tkt->Close_File();
	/*std::cerr<<"myunion size "<<myunion.size()<<std::endl;*/
	std::cerr<<"Partitioning started...  "<<myunion.size()<<std::endl;
	//接下来会显示每一个 union 检测后的 inter_flag 标志位
	for(int n = 0 ; n < myunion.size(); n++)
	{
		int inter_flag = 0;
		if(myunion[n].flag == 0)
		{
			Halfedge_handle h = myunion[n].handle;
			Halfedge_handle end = h;
			Halfedge_handle s,t,tmp;
			int line = 0 ;
			do{
				if(similarity(h->vertex()->point(),myunion[n].source))
				{
					if(inter_flag == 2)
					{
						inter_flag = 3;
						tmp = h;
					}
					else
						inter_flag = 1;	// 已切过source
				}
				if(similarity(h->vertex()->point(),myunion[n].target))
				{
					if(inter_flag == 1)
					{
						inter_flag = 3;
						tmp = h;
					}
					else 
					{
						inter_flag = 2;
					}
				}
				if(my_parallel(h->vertex()->point(),h->next()->vertex()->point(),myunion[n].source))
					s = h;
				if(my_parallel(h->vertex()->point(),h->next()->vertex()->point(),myunion[n].target))
					t = h;
				h = h->next();
				line ++;
			}while( h != end);
			if(h == NULL || s == NULL)
			{
				printf("Please low down precision...");
				system("pause");
			}
			if( inter_flag == 0 && line == 3)
			{
				h = m_pPolyhedron->split_edge(s->next());
				h->vertex()->point() = myunion[n].source;
				h = m_pPolyhedron->split_edge(t->next());
				h->vertex()->point() = myunion[n].target;
				m_pPolyhedron->split_facet(h,h->next()->next());
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
			else if (inter_flag == 1 && line == 4)
			{
				h = m_pPolyhedron->split_edge(t->next());
				h->vertex()->point() = myunion[n].target;
				m_pPolyhedron->split_facet(h,h->next()->next());
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
			else if (inter_flag == 2 && line == 4)
			{
				h = m_pPolyhedron->split_edge(s->next());
				h->vertex()->point() = myunion[n].source;
				m_pPolyhedron->split_facet(h,h->next()->next());
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
			else if( inter_flag == 3 && line == 5)
			{
				m_pPolyhedron->split_facet(tmp,tmp->next()->next());
				m_pPolyhedron->split_facet(tmp,tmp->next()->next());
			}		
			else
			{
				std::cerr<<inter_flag<<"    "<<line<<std::endl;
				return -9000;
			}
		}
		else if(myunion[n].flag == 1)
		{
			Halfedge_handle h = myunion[n].handle;
			Halfedge_handle end = h;
			Halfedge_handle s,t;
			do{
				if(similarity(h->vertex()->point(),myunion[n].target))
				{
					inter_flag = 1;	// source partitioned
					break;
				}
				if(my_parallel(h->vertex()->point(),h->next()->vertex()->point(),myunion[n].target))
					t = h;
				h = h->next();
			}while( h != end);
			if(h == NULL || s == NULL)
			{
				printf("Please low down precision...");
				system("pause");
			}
			if( inter_flag == 0)
			{
				h = m_pPolyhedron->split_edge(t->next());
				h->vertex()->point() = myunion[n].target;
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
			else if (inter_flag == 1)
			{
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
		}
		else if(myunion[n].flag == 2)
		{
			Halfedge_handle h = myunion[n].handle;
			Halfedge_handle end = h;
			Halfedge_handle s,t;
			do{
				if(similarity(h->vertex()->point(),myunion[n].source))
				{
					inter_flag = 1;	// source partitioned
					break;
				}
				if(my_parallel(h->vertex()->point(),h->next()->vertex()->point(),myunion[n].source))
					t = h;
				h = h->next();
			}while( h != end);
			if(h == NULL || s == NULL)
			{
				printf("Please low down precision...");
				system("pause");
			}
			if( inter_flag == 0)
			{
				h = m_pPolyhedron->split_edge(t->next());
				h->vertex()->point() = myunion[n].source;
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
			else if (inter_flag ==1)
			{
				m_pPolyhedron->split_facet(h,h->next()->next());
			}
		}
		else if(myunion[n].flag == 3)
		{
			;//Do nothing
		}
	}
	std::cerr<<"Partitioned successfully..."<<std::endl;

	for(Polyhedron::Facet_iterator it=m_pPolyhedron->facets_begin(),end=m_pPolyhedron->facets_end();it!=end;it++)
	{
		if(it->is_triangle())
			;
		else
		{
			std::cerr<<"Wrong !!! Non-tri !!!"<<std::endl;
			system("pause");
			return -9000;				//Bad partition return -9000 to avoid .
		}
	}
	if( !(pos_polyhedron->empty()&&neg_polyhedron->empty())){ pos_polyhedron->clear();neg_polyhedron->clear();}
	*pos_polyhedron = *m_pPolyhedron;
	*neg_polyhedron = *m_pPolyhedron;
	for(Polyhedron::Facet_iterator it=pos_polyhedron->facets_begin(),end=pos_polyhedron->facets_end();it!=end;it++)
	{
		Halfedge_handle h = it->halfedge();
		Halfedge_handle e = h;
		int m = 0;
		do{
			if(negative(plane,h->vertex()->point()))
			{
				pos_polyhedron->erase_facet(e);
				break;
			}
			h = h->next();
		}while(h != e);
	}

	for(Polyhedron::Facet_iterator it=neg_polyhedron->facets_begin(),end=neg_polyhedron->facets_end();it!=end;it++)
	{
		Halfedge_handle h = it->halfedge();
		Halfedge_handle e = h;
		int m = 0;
		do{
			if(positive(plane,h->vertex()->point()))
			{
				neg_polyhedron->erase_facet(e);
				break;
			}
			h = h->next();
		}while(h != e);
	}


	//计算AO值
	printf("AO SIZE %d\n",ao_points.size());
	cut_score += tkt->AO_Integrate(ao_points);
	printf("%.4f \n",cut_score);
	system("pause");

	cut_score+=bsp_insert(*neg_polyhedron,"F:\\tmp.off",btree);
	cut_score+=bsp_insert(*pos_polyhedron,"F:\\tmp.off",btree);
// 	Fillholes myfill;
// 	std::fstream neg_file("F:\\pos.off");
// 	std::fstream pos_file("F:\\neg.off");
// 	int holes = 0;
// 	//Operate the negative half-space polyhedron
// 	neg_file << *neg_polyhedron;
// 	tree->poly_hole = *neg_polyhedron;
// 	holes = myfill.fillholes("F:\\pos.off","F:\\pos.off");
// 	// We should insert more than one bsp tree in this case 
// 	if(holes > 1 )
// 	{
// 
// 	}
// 
// 	neg_file>>*neg_polyhedron;
// 	tree->poly = *neg_polyhedron;
// 	tree->OBB = 1.0 ;
// 	BSP_tree.insert(*tree);
// 	std::cerr<< holes<<std::endl;
// 
// 	//Operate the positive half-space polyhedron
// 	pos_file << *pos_polyhedron;
// 	tree->poly_hole = *pos_polyhedron;
// 	holes = myfill.fillholes("F:\\neg.off","F:\\neg.off");
// 	pos_file>> *m_pPolyhedron;
// 	tree->poly = *pos_polyhedron;
// 	tree->OBB = 2.0;
// 	BSP_tree.insert(*tree);
// 	//Close both files
// 	pos_file.close();
// 	neg_file.close();
// 	std::cerr<< holes<<std::endl;
// 	std::cerr<< "BSP_tree has " << BSP_tree.size() << " leafs" <<std::endl;
// 	std::cerr<<"Fill holes completed"<<std::endl;
	m_cut_plane = CUT_SEGMENTS;
	return cut_score;
}

void Scene::cutting_plane()
{
    switch( m_cut_plane )
    {
      case UNSIGNED_FACETS:
          return unsigned_distance_function();
      case SIGNED_FACETS:
          return signed_distance_function();
      case UNSIGNED_EDGES:
          return unsigned_distance_function_to_edges();
      case CUT_SEGMENTS:
          return cut_segment_plane();
      case NONE: // do nothing 
          return;
    }
    
    // Should not be here
    std::cerr << "Unknown cut_plane type" << std::endl;
    CGAL_assertion(false);
}

void Scene::toggle_view_poyhedron()
{
    m_view_polyhedron = !m_view_polyhedron;
}

void Scene::toggle_view_segments()
{
    m_view_segments = !m_view_segments;
}

void Scene::toggle_view_points()
{
    m_view_points = !m_view_points;
}

void Scene::toggle_view_plane()
{
    m_view_plane = !m_view_plane;
}

void Scene::refine_bisection(const FT max_sqlen)
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }
    std::cout << "Refine through recursive longest edge bisection...";
    Refiner<Kernel,Polyhedron> refiner(m_pPolyhedron);
    refiner(max_sqlen);
    std::cout << "done (" << m_pPolyhedron->size_of_facets() << " facets)" << std::endl;
  
    clear_internal_data();
}

void Scene::refine_loop()
{
    if(m_pPolyhedron == NULL)
    {
        std::cout << "Load polyhedron first." << std::endl;
        return;
    }
    std::cout << "Loop subdivision...";
    CGAL::Subdivision_method_3::Loop_subdivision(*m_pPolyhedron, 1);
    std::cout << "done (" << m_pPolyhedron->size_of_facets() << " facets)" << std::endl;
  
    clear_internal_data();
}

void Scene::activate_cutting_plane()
{
    connect(m_frame, SIGNAL(modified()), this, SLOT(cutting_plane()));
    m_view_plane = true;
}

void Scene::deactivate_cutting_plane()
{
    disconnect(m_frame, SIGNAL(modified()), this, SLOT(cutting_plane()));
    m_view_plane = false;
}

double Scene::bsp_insert(Polyhedron poly,char* dir,std::set<BSP>& btree)
{
	int temp_score = 0.0;
	std::ofstream file(dir);
	std::cerr<<dir<<std::endl;
	int holes = 0;
	//Operate the negative half-space polyhedron
	if(poly.size_of_vertices() < 10)
		return -9000;
	file << poly;
	std::ifstream file_in(dir);
	holes = eulerUpdate(poly);
	std::cerr<< "Holes : " << holes <<"........................."<<std::endl;
	// We should insert more than one bsp tree in this case
	if(holes > 1 )
	{
		Polyhedron poly_tmp ;
		file_in>>poly_tmp;
		eulerUpdate(poly_tmp);
		for(int i = 1 ; i <= holes ; i++)
		{
			Polyhedron poly_t = poly_tmp;
			//cut model with hole
			for(Polyhedron::Facet_iterator it = poly_t.facets_begin() , end = poly_t.facets_end() ;
				it != end ; it++)
			{
				if(it->mask != i)
				{
					if(it->halfedge()!=NULL)	
					{
						poly_t.erase_facet(it->halfedge());
						it = poly_t.facets_begin() , end = poly_t.facets_end();
					}
				}
			}
			tree->poly = poly_t;
			//cut model without hole
			Polyhedron poly_h = poly;
			for(Polyhedron::Facet_iterator it = poly_h.facets_begin() , end = poly_h.facets_end() ;
				it != end ; it++)
			{
				if(it->mask != i)
				{
					if(it->halfedge()!=NULL)	
					{
						poly_h.erase_facet(it->halfedge());
						it = poly_h.facets_begin() , end = poly_h.facets_end();
					}
				}
			}
			tree->poly_hole =  poly_h ; 
			//calculate volume
			tree->V = get_volume(poly_t);
			std::cerr<< "This leaf's  volume is ";
			//calculate obb
			tree->OBB = tkt->Volume_OBB(poly);
			//calculate score;
			srand(int(time(0)));
			tree->score = rand()%1000;
			std::cerr<< "This leaf's OBB volume is ";
			printf("%lf\n",tree->OBB);
			//insert to bsp tree
			if(GLOBAL_F)
			{
				if( tree->OBB < PRINTER_V  )
					result.push_back(*tree);
				else
					btree.insert(*tree);
			}
		}
	}
	else
	{
		tree->poly_hole = poly;						// with hole
		file_in>>poly;									// without hole
		tree->poly = poly;							// without hole
		tree->OBB = tkt->Volume_OBB(poly) ;		// calculate obb
		tree->V = get_volume(poly);			// calculate volume
			srand(int(time(0)));
			tree->score = rand()%1000;					// calculate score
		std::cerr<< "This leaf's  volume is ";
		printf("%lf\n",tree->V);
		std::cerr<< "This leaf's OBB volume is ";
		printf("%lf\n",tree->OBB);
		if(GLOBAL_F)
		{
			if( tree->OBB < PRINTER_V  )
				result.push_back(*tree);
			else
				btree.insert(*tree);
		}
	}
	file.close();
	file_in.close();
	return temp_score;
}

double Scene::get_aabb(Polyhedron poly_3) {
	typedef double                     FT;
	typedef CGAL::Simple_cartesian<FT> K;
	typedef K::Point_2                 Point_2;
	typedef K::Point_3                 Point_3;
	typedef CGAL::Exact_predicates_inexact_constructions_kernel  Kernel;
	typedef Polyhedron::Halfedge_handle                          Halfedge_handle;
	typedef Polyhedron::Halfedge_const_iterator					 Halfedge_iterator;
	std::vector<Point_3> points_3;
	Point_3 p_;
	for(Halfedge_iterator he = poly_3.halfedges_begin(),end = poly_3.halfedges_end();he != end; he++)
	{
		p_ = Point_3(he->vertex()->point().x(),he->vertex()->point().y(),he->vertex()->point().z());
		points_3.push_back(p_);
	}
	K::Iso_cuboid_3 c3 = CGAL::bounding_box(points_3.begin(), points_3.end());
	double volume = c3.volume();
	return volume;
}

int Scene::eulerUpdate(Polyhedron& P1)
{
	typedef CGAL::Exact_predicates_inexact_constructions_kernel  Kernel;
	typedef Polyhedron::Halfedge_const_iterator					 Halfedge_iterator;
	typedef Polyhedron::Edge_const_iterator						 Edge_iterator;
	typedef Facet::Halfedge_around_facet_const_circulator HF_circulator;
	int noc = 0;
	Facet_handle t , s;
	while(!trilist.empty()) {trilist.pop();}
	//所有的facet的mask都为0
	for(Facet_iterator it = P1.facets_begin() , end = P1.facets_end() ;
		it != end;it++)
	{
		if(!it->mask)
		{
			noc ++ ;
			trilist.push(it);
			it->mask = noc ;
			while(trilist.size())
			{
				t = trilist.top();
				trilist.pop();
				if((s=t->halfedge()->opposite()->facet()) != NULL && (!s->mask)) {trilist.push(s);s->mask=noc;}
				if((s=t->halfedge()->next()->opposite()->facet()) != NULL && (!s->mask)){trilist.push(s);s->mask=noc;}
				if((s=t->halfedge()->prev()->opposite()->facet()) != NULL && (!s->mask)){trilist.push(s);s->mask=noc;} 
			}
		}
		//std::cout << "Mask of facet is "<< it->mask <<std::endl;
	} 
	return noc;
}

double Scene::get_volume(Polyhedron poly_3)
{
	typedef Polyhedron::Halfedge_handle                          Halfedge_handle;
	typedef Polyhedron::Halfedge_const_iterator					 Halfedge_iterator;
	typedef Polyhedron::Edge_const_iterator						 Edge_iterator;
	typedef Facet::Halfedge_around_facet_const_circulator HF_circulator;
	if(poly_3.size_of_facets() < 5)
		return 0.0;
	double volume = 0.;
	for(Facet_handle it = poly_3.facets_begin(),itd = poly_3.facets_end();
		it != itd; it ++)
	{
		Vector normal , center , area;
		//Calculate triangle's normal
		normal = CGAL::NULL_VECTOR;
		HF_circulator he = it->facet_begin();
		HF_circulator end = he;

		const Point& prev = he->prev()->vertex()->point();
		const Point& curr = he->vertex()->point();
		const Point& next = he->next()->vertex()->point();
		center = Vector((prev.x()+curr.x()+next.x())/3 , (prev.y()+curr.y()+next.y())/3
			,(prev.z()+curr.z()+next.z())/3);
		area = CGAL::cross_product(next-curr,prev-curr) / 2;

		CGAL_For_all(he,end)
		{
			const Point& prev = he->prev()->vertex()->point();
			const Point& curr = he->vertex()->point();
			const Point& next = he->next()->vertex()->point();
			const Vector n = CGAL::cross_product(next-curr,prev-curr);
			normal = normal + n;
		}
		normal = normal / std::sqrt(normal * normal);
		volume  += (center*normal) * (std::sqrt(area.squared_length()));
	}
	return volume/3;
}